# Demo Lesson: Asynchronous Programming and the Event Loop in JavaScript

**Course Title:** Front-End Development: HTML/CSS/JavaScript  
**Demo Topic:** Asynchronous Programming/Event Loop  
**Language of Delivery:** English  
**Demo Duration:** 30 minutes followed by Q&A  
**Date:** June 25, Tuesday, 11 AM Guinea time  

---

## Agenda

1. [Introduction to Asynchronous Programming](#1-introduction-to-asynchronous-programming)
2. [Understanding the Event Loop](#2-understanding-the-event-loop)
3. [Examples and Hands-On Coding](#3-example-and-hands-on-coding) 
4. [Q&A Session](#4-qa-session) 

---

## 1. Introduction to Asynchronous Programming

### What is Asynchronous Programming?

Asynchronous programming allows the program to perform other tasks while waiting for operations like fetching data from a server, reading files, or waiting for a timer to complete.

### Synchronous vs Asynchronous Code

**Synchronous**: 
- Execution happens line by line.
- Each operation waits for the previous one to be done before moving to the next.
- Can lead to blocking the main thread, where a slow operation holds up the entire program.

**Asynchronous**: 
- Execution can continue without waiting for an operation to complete.
- Uses mechanisms like callbacks, promises, and async/await to handle operations that take time.
- Prevents blocking the main thread by allowing the program to perform other tasks while waiting for the completion of long-running operations.

## Some crutial points of asynchronicity in JavaScript:
   ### 1. Non-Blocking:
        Long operations like network calls or database access can block the main execution thread if handled synchronously.
        Asynchronous programming allows the program to continue running while these operations are ongoing, avoiding blocking the user interface and improving responsiveness.
   ### 2. Event Handling:
    JavaScript is commonly used to handle user events like clicks or keystrokes.
    Asynchronous programming allows these events to be processed efficiently without interrupting the main execution flow.
## 2. Understanding the Event Loop

### Call Stack

The call stack is a mechanism for an interpreter to keep track of its place in a script that calls multiple functions.

### Web APIs

JavaScript interacts with Web APIs (e.g., setTimeout, fetch, DOM events) to handle asynchronous operations.

### Task Queue

The task queue (or callback queue) holds messages (callbacks or tasks) that are to be processed.

### Event Loop Mechanism

The event loop is a constantly running process that monitors both the call stack and the task queue. If the call stack is empty, it takes the first task from the task queue and pushes it to the call stack for execution.
Example 1
```javascript
console.log('Start');

setTimeout(() => {
  console.log('Timeout');
}, 2000);

console.log('End');
```

**Expected Output:**

```plaintext
Start
End
Timeout
```

Example 2
``` javascript
  console.log('Start...');

  document.getElementById("btn")
  .addEventListener("click", function cb(){
    console.log("Callback");
  })

  console.log('End...');

```
**Expected Output:**

```plaintext
Start...
End...
Callback
```


Example 2
``` javascript
  console.log('Execution Start...');
  setTimeout(function(){
    console.log("SetTimeout program executed...")
  }, 0);
  Promise.resolve("Promise resolved data...")
  .then(function(data){
    console.log(data);
  })
  console.log('Execution End...');

```
**Expected Output:**

```plaintext
Execution Start...
Execution End...
Promise resolved data...
SetTimeout program executed...
```
## JavaScript Visualizer 9000 link
 - https://www.jsv9000.app/

### Asynchronous Functions in JavaScript

Functions that operate asynchronously are central to modern web applications.

### Callbacks

Functions passed as arguments to other functions that execute once an asynchronous operation completes.

### Promises

Objects representing the eventual completion or failure of an asynchronous operation.

### Async/Await

Syntax for writing asynchronous code in a more synchronous fashion, making it easier to read and maintain.

## 3. Examples and Hands-On Coding


### Creating and Resolving Promises

```javascript
let promise = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('Resolved!');
  }, 3000);
});

promise.then((message) => {
  console.log(message);
});
```

### Using fetch API with Async/Await

```javascript
async function fetchData() {
  try {
    let response = await fetch('https://jsonplaceholder.typicode.com/posts/1');
    let data = await response.json();
    console.log(data);
  } catch (error) {
    console.error('Error fetching data:', error);
  }
}

fetchData();
```
## Playground link
 - https://playcode.io/javascript


### Debugging Techniques

- Using `console.log` statements to trace asynchronous code execution.
- Using debugging tools in browsers to step through asynchronous code.

### Best Practices

- Use Promises and async/await for cleaner and more manageable asynchronous code.
- Always handle errors using `.catch` with Promises or `try/catch` blocks with async/await.

## Conclusion

Understanding asynchronous programming and the event loop is crucial for front-end developers. This knowledge enables you to write efficient, non-blocking code, enhancing the user experience in web applications. Through practical examples and hands-on coding, you'll gain the skills needed to manage asynchronous operations effectively.

## 4. Q&A Session

Feel free to ask any questions or request further clarification on asynchronous programming concepts or specific code examples.

## Instructor

- **Name:** Souleymane 1 DIALLO
- **Contact:** souleymane1dialo@gmail.com | +224622639167
- **LinkedIn:** https://www.linkedin.com/in/souleymane-1-diallo-8119a3195/

---

## Feedback

Your feedback is valuable ! Please feel free to reach out with any questions, suggestions, or comments regarding this demo lesson.

